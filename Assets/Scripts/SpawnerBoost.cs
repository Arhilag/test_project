using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerBoost : MonoBehaviour
{
    [SerializeField] private float time;
    [SerializeField] private GameObject[] boosters;
    private bool game = false;

    public void StartBoost()
    {
        game = true;
        StartCoroutine(Spawn());
    }

    private IEnumerator Spawn()
    {
        while (game == true)
        {
            GameObject boster = boosters[Random.Range(0, boosters.Length)];
            yield return new WaitForSeconds(time);
            var pawn = Instantiate(boster, transform.position + new Vector3(Random.Range(-4.5f, 4.5f),
                0, Random.Range(-4.5f, 4.5f)), transform.rotation);
            
        }
    }

    public void StopBoost()
    {
        game = false;
    }
}
