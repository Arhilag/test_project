using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Booster : MonoBehaviour
{
    [SerializeField] private bool time;
    [SerializeField] private float addTime;
    private Spawner spawner;
    private void Start()
    {
        spawner = FindObjectOfType<Spawner>();
    }

    private void OnMouseDown()
    {
        var script = spawner.GetComponent<Spawner>();
        if(time == true) script.AddTime(addTime);
        else script.Refresh();
        Destroy(gameObject);
    }
}
