using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Data_Monster : MonoBehaviour
{
    [SerializeField] private float health;
    public float time;
    [SerializeField] private float speed;
    private Spawner spawner;

    private void Start()
    {
        spawner = FindObjectOfType<Spawner>();
        transform.rotation = Quaternion.Euler(0,Random.Range(0,359),0);
    }

    private void OnMouseDown()
    {
        health--;
        if (health <= 0)
        {
            spawner.DeliteEnemy();
            Destroy(gameObject);
        }
    }

    private void FixedUpdate()
    {
        transform.position += transform.forward * speed;
        if(transform.position.x <= -4.5 || transform.position.x >= 4.5 || 
            transform.position.z <= -4.5 || transform.position.z >= 4.5)
        {
            transform.rotation *= Quaternion.Euler(0, 90, 0);
        }
    }

    public void UpComplexiti(float dx)
    {
        health *= dx;
        speed *= dx;
    }
}
