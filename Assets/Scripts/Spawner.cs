using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    private GameObject monster;
    [SerializeField] private GameObject[] monsters;
    [SerializeField] private GameObject particle;
    [SerializeField] private int count_monsters;
    private float time;
    private bool game = false;
    [SerializeField] private GameObject canvas;
    [SerializeField] private float dx;
    private float dp = 1;
    private SpawnerBoost spawnerBoost;
    private void Start()
    {
        spawnerBoost = FindObjectOfType<SpawnerBoost>();
    }
    public void StartGame()
    {
        canvas.SetActive(false);
        game = true;
        dp = 1;
        StartCoroutine(Spawn());
        StartCoroutine(Complex());
    }

    private IEnumerator Spawn()
    {
        while (game == true)
        {
            monster = monsters[Random.Range(0, monsters.Length)];
            var cscript = monster.GetComponent<Data_Monster>();
            time = cscript.time;
            yield return new WaitForSeconds(time);
            var pawn = Instantiate(monster, transform.position + new Vector3(Random.Range(-4.5f,4.5f), 
                0, Random.Range(-4.5f, 4.5f)), transform.rotation);
            Instantiate(particle, pawn.transform.position, transform.rotation);
            var script = pawn.GetComponent<Data_Monster>();
            script.UpComplexiti(dp);
            count_monsters++;
            if (count_monsters == 10) Loss();
        }
    }

    public void DeliteEnemy()
    {
        count_monsters--;
    }

    private void Loss()
    {
        game = false;
        spawnerBoost.StopBoost();
        canvas.SetActive(true);
        Debug.Log("Поражение!");
    }

    private IEnumerator Complex()
    {
        while (true)
        {
            yield return new WaitForSeconds(3);
            dp *= dx;
        }
    }

    public void Refresh()
    {
        var obj = GameObject.FindGameObjectsWithTag("monster");
        count_monsters -= obj.Length;
        for (int i = 0; i < obj.Length; i++)
        {
            Destroy(obj[i]);
        }
        
    }

    public void AddTime(float add)
    {
        time += add;
    }
}
